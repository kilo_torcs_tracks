Track name: Corkscrew

A racetrack near Monterey, California,
with the infamous Corkscrew turn complex.

A complete version of this track, v0.4 + 3D work by Andrew Sumner
is available in the Speed Dreams repositories.
http://speed-dreams.org/

License:
All XML, PNG, XCF, JPG, AC, ACC, RGB files for this track are:
Copyright 2009 Gabor Kmetyko <kg.kilo&gmail.com>
Copyleft: This is a free work, you can copy, distribute,
and modify it under the terms of the Free Art License
http://artlibre.org/licence/lal/en/
