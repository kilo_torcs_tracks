Copyright � 2007 kilo aka Gabor Kmetyko

A TORCS road racetrack, based on a slotcar track design.
All turns are 45 degrees or multiples of that.

Copyleft: this work of art is free, you can redistribute
it and/or modify it according to terms of the Free Art license.
You will find a specimen of this license on the site
Copyleft Attitude http://artlibre.org as well as on other sites.

